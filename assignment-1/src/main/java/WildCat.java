public class WildCat {

    // TODO Complete me!
    String name;
    double weight; // In kilograms
    double length; // In centimeters
	
	public void setname(String name){
		this.name = name;
	}
	public void setweight(double weight){
		this.weight = weight;
	}
	public void setlength(double length){
		this.length = length;
	}
	public String getname(){
		return this.name;
	}
	public double getlength(){
		return this.length;
	}
	public double getweight(){
		return this.weight;
	}
	
    public WildCat(String name, double weight, double length) {
        this.name = name;
		this.weight = weight;
		this.length = length;
    }

    public double computeMassIndex() {
        double massIndex = (this.weight/Math.pow(this.length/100,2));
		return massIndex;
    }
}
