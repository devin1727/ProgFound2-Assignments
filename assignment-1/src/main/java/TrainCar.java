public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

	private WildCat isi;
	private double beratIsi;
	private boolean sambungan;
	private TrainCar gerbong;
	
	public WildCat getisi(){
		return this.isi;
	}
	public double getberatIsi(){
		return this.beratIsi;
	}
	public TrainCar getGerbong(){
		return this.gerbong;
	}
	public void setGerbong(TrainCar param){
		this.gerbong = param;
	}
    public TrainCar(WildCat cat) {
		this.isi = cat;
		this.beratIsi = cat.weight + EMPTY_WEIGHT;
		this.sambungan = false;
		this.gerbong = null;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.isi = cat;
		this.beratIsi = cat.weight + EMPTY_WEIGHT;
		this.gerbong = next;
		this.sambungan = true;
    }

    public double computeTotalWeight() {
        if (this.gerbong == null){
			return (this.beratIsi);
		}
		else {
			return (this.beratIsi + this.gerbong.computeTotalWeight());
		}
    }

    public double computeTotalMassIndex() {
        if (this.gerbong == null){
			return (this.isi.computeMassIndex());
		}
		else {
			return (this.isi.computeMassIndex() + this.gerbong.computeTotalMassIndex());
		}
	}

    public void printCar() {
        if (this.gerbong==null){
			System.out.print("--(" + this.isi.name + ")");
		}
		else {
			System.out.print("--(" + this.isi.name + ")");
			this.gerbong.printCar();
		}
    }
}