import java.util.Scanner;
import java.util.*;

public class A1Station {
	

	public static ArrayList<TrainCar> obj = new ArrayList<TrainCar>();
	
    private static final double THRESHOLD = 250; // in kilograms


    public static void main(String[] args) {
		double x = 0;
		Scanner reader = new Scanner(System.in);
        int input = reader.nextInt();
		
		for (int i = 0;i < input;i++){
			String kucing = reader.next();
			String[] data = kucing.split(",");
			String nama = data[0];
			double berat = Double.parseDouble(data[1]);
			double panjang = Double.parseDouble(data[2]);
			WildCat cat = new WildCat(nama,berat,panjang);
			TrainCar kereta = new TrainCar(cat);
			obj.add(kereta);
			x += obj.get(obj.size()-1).computeTotalWeight();
		
			if (x > THRESHOLD || i == (input-1)) {
				for (int j = obj.size()-1; j > 0; j--){
					obj.get(j).setGerbong(obj.get(j-1));
				}
				System.out.println("The train departs to Javari Park");
				System.out.print("[LOCO]<");
				obj.get(obj.size()-1).printCar();
				System.out.println(obj.get(obj.size()-1).computeTotalMassIndex()/obj.size());
				obj.clear();
				x = 0;
			}
	
		}
		
    }
}