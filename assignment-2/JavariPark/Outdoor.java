import java.util.*;
public class Outdoor{
	private ArrayList<ArrayList<Cage>> listLion;
	private ArrayList<ArrayList<Cage>> listEagle;

	public ArrayList<ArrayList<Cage>> getLion(){
		return listLion;
	}
	public ArrayList<ArrayList<Cage>> getEagle(){
		return listEagle;
	}
	public void addLion(String param){
		listLion = new ArrayList<ArrayList<Cage>>();
		String[] namasplit = param.split(",");
		int y = namasplit.length/3;
		int x = 0;
		if (y >= 1){
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Lion(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				if (listLion.size() == 0){
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listLion.add(pertama);
				}
				else if (listLion.get(x).size() < y){
					listLion.get(x).add(kandang);
				}
				else if (x == 2){
					listLion.get(2).add(kandang);
				}
				else {
					x+=1;
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listLion.add(pertama);
				}
			}
		}
		else {
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Lion(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				ArrayList<Cage> pertama = new ArrayList<Cage>();
				pertama.add(kandang);
				listLion.add(pertama);
				x+=1;
			}
		}
	}
	public void addEagle(String param){
		listEagle = new ArrayList<ArrayList<Cage>>();
		String[] namasplit = param.split(",");
		int y = namasplit.length/3;
		int x = 0;
		if (y >= 1){
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Eagle(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				if (listEagle.size() == 0){
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listEagle.add(pertama);
				}
				else if (listEagle.get(x).size() < y){
					listEagle.get(x).add(kandang);
				}
				else if (x == 2){
					listEagle.get(2).add(kandang);
				}
				else {
					x+=1;
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listEagle.add(pertama);
				}
			}
		}
		else {
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Eagle(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				ArrayList<Cage> pertama = new ArrayList<Cage>();
				pertama.add(kandang);
				listEagle.add(pertama);
				x+=1;
			}
		}
	}
	public void printArrange(ArrayList<ArrayList<Cage>> list){
		Animal isi = list.get(0).get(0).getIsi();
		if (isi.isPet()){
			System.out.println("Location: Indoor");
		}
		else {
			System.out.println("Location: Outdoor");
		}
		int x = 1;
		for (ArrayList<Cage> d:list){
			System.out.print("Level "+x + ": ");
			for (Cage f:d){
				Animal bla = f.getIsi();
				String type = f.getJenis();
				String nama = bla.getNama();
				int length = bla.getPanjang();
				System.out.print(nama + "(" +length + " - "+type+"), " );
			}
			System.out.print("\n");
			x+=1;
		}
	}
	public void Rearrange(ArrayList<ArrayList<Cage>> list){
		int besar = list.size();
		if (besar > 0){
			ArrayList<Cage> toMove = list.get(2);
			ArrayList<Cage> toMove1 = list.get(0);
			list.set(0, toMove);
			list.set(2, toMove1);
			for (int i = 0;besar > i;i++){
				for (int j = 0;list.get(i).size() > j;j++){
					int x = list.get(i).size()/2;
					int m = list.get(i).size()-1;
					for (int k = 0; x > k; k++){
						Cage pindah = list.get(i).get(k);
						Cage pindah1 = list.get(i).get(m);
						list.get(i).set(k,pindah1);
						list.get(i).set(m,pindah);
						m--;
					}
				}
			}
		}
	}	
}