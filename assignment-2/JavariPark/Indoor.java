import java.util.*;
public class Indoor{
	private ArrayList<ArrayList<Cage>> listCat;
	private ArrayList<ArrayList<Cage>> listParrot;
	private ArrayList<ArrayList<Cage>> listHamster;

	public ArrayList<ArrayList<Cage>> getCat(){
		return listCat;
	}
	public ArrayList<ArrayList<Cage>> getParrot(){
		return listParrot;
	}
	public ArrayList<ArrayList<Cage>> getHamster(){
		return listHamster;
	}
	public void addCat(String param){
		listCat = new ArrayList<ArrayList<Cage>>();
		String[] namasplit = param.split(",");
		int y = namasplit.length/3;
		int x = 0;
		if (y >= 1){
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Cat(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				if (listCat.size() == 0){
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listCat.add(pertama);
				}
				else if (listCat.get(x).size() < y){
					listCat.get(x).add(kandang);
				}
				else if (x == 2){
					listCat.get(2).add(kandang);
				}
				else {
					x+=1;
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listCat.add(pertama);
				}
			}
		}
		else {
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Cat(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				ArrayList<Cage> pertama = new ArrayList<Cage>();
				pertama.add(kandang);
				listCat.add(pertama);
				x+=1;
			}
		}	
	}
	public void addHamster(String param){
		listHamster = new ArrayList<ArrayList<Cage>>();
		String[] namasplit = param.split(",");
		int y = namasplit.length/3;
		int x = 0;
		if (y >= 1){
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Hamster(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				if (listHamster.size() == 0){
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listHamster.add(pertama);
				}
				else if (listHamster.get(x).size() < y){
					listHamster.get(x).add(kandang);
				}
				else if (x == 2){
					listHamster.get(2).add(kandang);
				}
				else {
					x+=1;
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listHamster.add(pertama);
				}
			}
		}
		else {
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Hamster(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				ArrayList<Cage> pertama = new ArrayList<Cage>();
				pertama.add(kandang);
				listHamster.add(pertama);
				x+=1;
			}
		}
	}
	public void addParrot(String param){
		listParrot = new ArrayList<ArrayList<Cage>>();
		String[] namasplit = param.split(",");
		int y = namasplit.length/3;
		int x = 0;
		if (y >= 1){
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Parrot(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				if (listParrot.size() == 0){
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listParrot.add(pertama);
				}
				else if (listParrot.get(x).size() < y){
					listParrot.get(x).add(kandang);
				}
				else if (x == 2){
					listParrot.get(2).add(kandang);
				}
				else {
					x+=1;
					ArrayList<Cage> pertama = new ArrayList<Cage>();
					pertama.add(kandang);
					listParrot.add(pertama);
				}
			}
		}
		else {
			for (String d:namasplit){
				String[] kucing = d.split("\\|");
				Animal hewan = new Parrot(kucing[0],Integer.parseInt(kucing[1]),true);
				Cage kandang = new Cage(hewan);
				ArrayList<Cage> pertama = new ArrayList<Cage>();
				pertama.add(kandang);
				listParrot.add(pertama);
				x+=1;
			}
		}
	}
	public void printArrange(ArrayList<ArrayList<Cage>> list){
		Animal isi = list.get(0).get(0).getIsi();
		if (isi.isPet()){
			System.out.println("Location: Indoor");
		}
		else {
			System.out.println("Location: Outdoor");
		}
		int x = 1;
		for (ArrayList<Cage> d:list){
			System.out.print("Level "+x + ": ");
			for (Cage f:d){
				Animal bla = f.getIsi();
				String type = f.getJenis();
				String nama = bla.getNama();
				int length = bla.getPanjang();
				System.out.print(nama + "(" +length + " - "+type+"), " );
			}
			System.out.print("\n");
			x+=1;
		}
	}
	public void Rearrange(ArrayList<ArrayList<Cage>> list){
		int besar = list.size();
		if (besar > 0){
			ArrayList<Cage> toMove = list.get(2);
			ArrayList<Cage> toMove1 = list.get(0);
			list.set(0, toMove);
			list.set(2, toMove1);
			for (int i = 0;besar > i;i++){
				for (int j = 0;list.get(i).size() > j;j++){
					int x = list.get(i).size()/2;
					int m = list.get(i).size()-1;
					for (int k = 0; x > k; k++){
						Cage pindah = list.get(i).get(k);
						Cage pindah1 = list.get(i).get(m);
						list.get(i).set(k,pindah1);
						list.get(i).set(m,pindah);
						m--;
					}
				}
			}
		}
	}
}