public class Cage{
	private Animal isi;
	private String jenis;

	public Animal getIsi(){
		return this.isi;
	}
	public String getJenis(){
		return this.jenis;
	}
	public Cage(Animal isi){
		this.isi = isi;
		int panjang = isi.getPanjang();
		if (isi.isPet()){
			if (panjang < 45) {
				this.jenis = "A";
			}
			else if (panjang >= 45 & panjang <= 60){
				this.jenis = "B";
			}
			else {
				this.jenis = "C";
			}
		}
		else {
			if (panjang < 75) {
				this.jenis = "A";
			}
			else if (panjang >= 75 & panjang <= 90){
				this.jenis = "B";
			}
			else {
				this.jenis = "C";
			}
		}
	}
}
