import java.util.Random;
import java.util.ArrayList;

public class Animal{
    private String nama;
    private int panjang;
    private boolean pet;

    public String getNama(){
		return this.nama;
	}
	public int getPanjang(){
		return this.panjang;
	}
	public boolean isPet(){
		return this.pet;
	}

    public Animal(String nama,int panjang,boolean pet){
    	this.nama = nama;
    	this.panjang = panjang;
    	this.pet = pet;
    }
    public void nonsense(){
		System.out.println("You do nothing...");
	}
}

class Cat extends Animal{
	private String suara_brush = "Nyaaan....";
    private String[] suara_cuddle = {"Miaaaw..","Purrr..","Mwaw!","Mraaawr!"};

    public Cat(String nama,int panjang,boolean pet){
    	super(nama,panjang,true);
    }

    public void brush(){
    	System.out.println("Time to clean " + this.getNama() + "'s fur");
    	System.out.println(this.getNama() + " makes a voice:"+ suara_brush);
    }
    public void cuddle(){
    	Random rand = new Random();
    	System.out.println(this.getNama() + " makes a voice " + suara_cuddle[rand.nextInt(4)]);
    }
}

class Lion extends Animal{
	private String suara_hunt = "err...!";
	private String suara_clean = "Hauhhmm!";
	
	public Lion(String nama, int panjang,boolean pet){
		super(nama,panjang,false);
	}
	public void hunt(){
		System.out.println(this.getNama() + " makes a voice: " + suara_hunt);
	}
	public void clean_Mane(){
		System.out.println(this.getNama() + " makes a voice: " + suara_clean);
	}
	public void disturb(){
		System.out.println(this.getNama() + " makes a voice: " + suara_clean.toUpperCase());
	}
}

class Hamster extends Animal{
	private String suara_gnaw = "ngkkrit.. ngkkrrriiit";
	private String suara_wheel = "trrr.... trrr....";
	
	
	public Hamster(String nama, int panjang,boolean pet){
		super(nama,panjang,true);
	}
	public void gnaw(){
		System.out.println(this.getNama() + " makes a voice: " + suara_gnaw);
	}
	public void run(){
		System.out.println(this.getNama() + " makes a voice: " + suara_wheel);
	}
}

class Eagle extends Animal{
	private String suara = "kwaakk...";
	
	public Eagle(String nama, int panjang,boolean pet){
		super(nama,panjang,false);
	}
	public void fly(){
		System.out.println(this.getNama() + " makes a voice: " + suara);
		System.out.println("You hurt");
	}
}

class Parrot extends Animal{
	private String suara_terbang = "TERBAAAANG....";

	public Parrot(String nama, int panjang,boolean pet){
		super(nama,panjang,true);
	}
	public void fly(){
		System.out.println("Parrot " + this.getNama() + " flies!");
		System.out.println(this.getNama()+ " makes a voice: " + suara_terbang);
	}
	public void bicara(String kalimat){
		System.out.println(this.getNama() + " says: " + kalimat.toUpperCase());
	}
	public void nonsense(){
		System.out.println(this.getNama() + " makes a voice: " + "HM?");
	}
}