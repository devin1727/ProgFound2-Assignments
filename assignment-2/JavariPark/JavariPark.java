import java.util.*;
public class JavariPark{

	public static void main(String[] args){
		ArrayList<Animal> felix;
		ArrayList<Animal> singa;
		ArrayList<Animal> elang;
		ArrayList<Animal> burung;
		ArrayList<Animal> tikus;
		felix = new ArrayList<Animal>();
		singa = new ArrayList<Animal>();
		elang = new ArrayList<Animal>();
		burung = new ArrayList<Animal>();
		tikus = new ArrayList<Animal>();
		Scanner in = new Scanner(System.in);
		JavariPark hutan = new JavariPark();
		Indoor dalam = new Indoor();
		Outdoor luar = new Outdoor();
		System.out.println("Welcome to Javari Park!");
		System.out.println("Input number of animals");
		System.out.println("Cat: ");
		String n = in.nextLine();
		if (Integer.parseInt(n) > 0){
			String kumpulan = in.nextLine();
			dalam.addCat(kumpulan);
			String[] kumpulansplit = kumpulan.split(",");
			for (String d:kumpulansplit){
				String[] kucing = d.split("\\|");
				Cat hewan = new Cat(kucing[0],Integer.parseInt(kucing[1]),true);
				felix.add(hewan);
			}
		}
		System.out.println("Lion: ");
		String a = in.nextLine();
		if (Integer.parseInt(a) > 0){
			String kumpulan = in.nextLine();
			luar.addLion(kumpulan);
			String[] kumpulansplit = kumpulan.split(",");
			for (String x:kumpulansplit){
				String[] kucing = x.split("\\|");
				Lion hewan = new Lion(kucing[0],Integer.parseInt(kucing[1]),false);
				singa.add(hewan);
			}
		}
		System.out.println("Eagle: ");
		String b = in.nextLine();
		if (Integer.parseInt(b) > 0){
			String kumpulan = in.nextLine();
			luar.addEagle(kumpulan);
			String[] kumpulansplit = kumpulan.split(",");
			for (String q:kumpulansplit){
				String[] kucing = q.split("\\|");
				Eagle hewan = new Eagle(kucing[0],Integer.parseInt(kucing[1]),false);
				elang.add(hewan);
			}
		}
		System.out.println("Parrot: ");
		String c = in.nextLine();
		if (Integer.parseInt(c) > 0){
			String kumpulan = in.nextLine();
			dalam.addParrot(kumpulan);
			String[] kumpulansplit = kumpulan.split(",");
			for (String p:kumpulansplit){
				String[] kucing = p.split("\\|");
				Parrot hewan = new Parrot(kucing[0],Integer.parseInt(kucing[1]),true);
				burung.add(hewan);
			}
		}
		System.out.println("Hamster: ");
		String d = in.nextLine();
		if (Integer.parseInt(d) > 0){
			String kumpulan = in.nextLine();
			dalam.addHamster(kumpulan);
			String[] kumpulansplit = kumpulan.split(",");
			for (String z:kumpulansplit){
				String[] kucing = z.split("\\|");
				Hamster hewan = new Hamster(kucing[0],Integer.parseInt(kucing[1]),true);
				tikus.add(hewan);
			}
		}
		System.out.println("Animals has been successfully recorded! \n=============================================");
		System.out.println("Cage arrangement: ");
		if (Integer.parseInt(n) > 0){
			dalam.printArrange(dalam.getCat());
			dalam.Rearrange(dalam.getCat());
			dalam.printArrange(dalam.getCat());
		}
		if (Integer.parseInt(a) > 0){
			luar.printArrange(luar.getLion());
			luar.Rearrange(luar.getLion());
			luar.printArrange(luar.getLion());
		}
		if (Integer.parseInt(b) > 0){
			luar.printArrange(luar.getEagle());
			luar.Rearrange(luar.getEagle());
			luar.printArrange(luar.getEagle());
		}
		if (Integer.parseInt(c) > 0){
			dalam.printArrange(dalam.getParrot());
			dalam.Rearrange(dalam.getParrot());
			dalam.printArrange(dalam.getParrot());
		}
		if (Integer.parseInt(d) > 0){
			dalam.printArrange(dalam.getHamster());
			dalam.Rearrange(dalam.getHamster());
			dalam.printArrange(dalam.getHamster());
		}
		System.out.println("ANIMALS NUMBER: ");
		System.out.println("Cat: "+n);
		System.out.println("Lion: "+a);
		System.out.println("Eagle: "+b);
		System.out.println("Parrot: "+c);
		System.out.println("Hamster: "+d);
		System.out.println("=============================================");
		while (true){
			System.out.println("Which animal you want to visit?\n (1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
			n = in.nextLine();
			boolean ada = false;
			if (n.equals("1")){
				System.out.print("Mention the name of cat you want to visit: ");
				n = in.nextLine();
				for (Animal x:felix) {
                    if (x.getNama().equals(n)) {
                        System.out.println("You are visiting " + n + " (cat) now, what would you like to do?");
                        System.out.println("1: Brush the fur 2: Cuddle");
                        int perintah = in.nextInt();
                        Cat y = (Cat) x;
                        if (perintah == 1) {
                            y.brush();
                            System.out.println("Back to the office!\n");
                        } else if (perintah == 2) {
                            y.cuddle();
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("You do nothing...");
                            System.out.println("Back to the office!\n");
                        }
                        ada = true;
                    }
                }
                if (!ada){
				System.out.println("There is no cat with that name!\nBack to the office!\n");
				}
			}
			else if(n.equals("2")){
				System.out.print("Mention the name of eagle you want to visit: ");
                n = in.nextLine();
                ada = false;
                for (Animal x : elang) {
                    if (x.getNama().equals(n)) {
                        System.out.println("You are visiting " + n + " (eagle) now, what would you like to do?");
                        System.out.println("1: Order to fly");
                        int perintah = in.nextInt();
                        Eagle y = (Eagle) x;
                        if (perintah == 1) {
                            y.fly();
                            System.out.println("You hurt!");
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("You do nothing...");
                            System.out.println("Back to the office!\n");
                        }
                       	ada = true;
                    }
                }
                if (!ada) {
                    System.out.println("There is no eagle with that name!\nBack to the office!\n");
                }
            }
            else if (n.equals("3")) { //visit hamster
                System.out.print("Mention the name of hamster you want to visit: ");
                n = in.nextLine();
                ada = false;
                for (Animal x : tikus) {
                    if (x.getNama().equals(n)) {
                        System.out.println("You are visiting " + n + " (eagle) now, what would you like to do?");
                        System.out.println("1: See it gnawning 2: Order to run in the hamster wheel");
                        int perintah = in.nextInt();
                        Hamster y = (Hamster) x;
                        if (perintah == 1) {
                            y.gnaw();
                            System.out.println("Back to the office!\n");

                        } else if (perintah == 2) {
                            y.run();
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("You do nothing...");
                            System.out.println("Back to the office!\n");
                        }
                        ada = true;
                    }
                }
                if (!ada) {
                    System.out.println("There is no hamster with that name!\nBack to the office!\n");
                }
            }
            else if (n.equals("4")) { //visit parrot
                System.out.print("Mention the name of parrot you want to visit: ");
                n = in.next();
                ada = false;
                for (Animal x : burung) {
                    if (x.getNama().equals(n)) {
                        System.out.println("You are visiting " + n + " (eagle) now, what would you like to do?");
                        System.out.println("1: Order to fly 2: Do conversation");
                        int perintah = in.nextInt();
                        Parrot y = (Parrot) x;
                        if (perintah == 1) {
                            y.fly();
                            System.out.println("Back to the office!\n");

                        } else if (perintah == 2) {
                            System.out.println("You say: ");
                            String conv = in.nextLine();
                            y.bicara(conv);
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("HM?");
                            System.out.println("Back to the office!\n");
                        }
                        ada= true;
                    }
                }
                if (!ada) {
                    System.out.println("There is no parrot with that name!\nBack to the office!\n");
                }
            }
            else if (n.equals("5")) {//visit lion
                System.out.print("Mention the name of lion you want to visit: ");
                n = in.next();
                for (Animal x : singa) {
                    if (x.getNama().equals(n)) {
                        System.out.println("You are visiting " + n + " (eagle) now, what would you like to do?");
                        System.out.println("1: Order to fly 2: Do conversation");
                        int perintah = in.nextInt();
                        ada = false;
                        Lion y = (Lion) x;
                        if (perintah == 1) {
                            y.hunt();
                            System.out.println("Back to the office!\n");

                        } else if (perintah == 2) {
                            y.clean_Mane();
                            System.out.println("Back to the office!\n");
                        } else if (perintah == 3) {
                            y.disturb();
                            System.out.println("Back to the office!\n");
                        } else {
                            System.out.println("You do nothing...");
                            System.out.println("Back to the office!\n");
                        }
                        ada = true;
                    }
                }
                if (!ada) {
                    System.out.println("There is no lion with that name!\nBack to the office!\n");
                }
            }
            else {
                break;
            }
		}
	}
}